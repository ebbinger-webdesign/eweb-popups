(function ($) {

    const $close_button = $('.eweb_popups_close');
    const $ok_button = $('.eweb_popups_button:not([href])');
    const $background = $('.eweb_popups_background');
  
    const $close_elements = $close_button.add($ok_button).add($background);
  
    $close_elements.on("click keypress", function (event) {
      // Only Enter for Keyboard
      if (event.type === "keypress" && event.which !== 13) {
        return;
      }
  
      var wrapper = $(this).closest(".eweb_popups_wrapper");
      wrapper.remove();
      event.preventDefault();
    });
  
  })(jQuery);
# EWEB Popups

A WordPress Plugin for displaying simple popups in PHP.

## Features

- Responsive Design
- Title and HTML Content
- Optional Call-To-Action Button

## Usage

Call the function:
```php
EWEB\Popups::show($title, $content, $button_label="", $button_url="", $new_tab=false, $class="");
```

### Parameters
- `$title : string` - The popup's title. Cannot contain HTML.
- `$content : string (HTML)` - The content section. You can use HTML Code. It is automatically escaped with wp_kses_post().
- `$button_label : string` - An optional button's title. Cannot contain HTML. The button will only be shown when `$button_label` is set.
- `$button_url : string (url)` - The button's destination URL. If unset, the button will close the popup.
- `$new_tab : bool` - If true, the button will open a new tab. Unused if `$button_url` is unset.
- `$class : string (Attribute)` - An additional class for the popup's wrapper.
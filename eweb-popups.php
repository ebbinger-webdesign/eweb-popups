<?php

/**
 * Plugin Name: EWEB Popups
 * Description: Display simple popups in PHP.
 * Version: 1.1
 * Author: Ebbinger Webdesign
 * Author URI: https://ebbinger.com
 * License: GPL v3 or later
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: eweb-popups
 * Domain Path: /languages
 */

namespace EWEB;

class Popups {

    /**
     * Displays a popup.
     * 
     * @param string $title The popup's title. Cannot contain HTML.
     * @param string $content The content section. You can use HTML Code. It is automatically escaped with wp_kses_post().
     * @param string $button_label An optional button's title. Cannot contain HTML. The button will only be shown when `$button_label` is set.
     * @param string $button_url The button's destination URL. If unset, the button will close the popup.
     * @param bool $new_tab If true, the button will open a new tab. Unused if `$button_url` is unset.
     * @param string $class An additional class for the popup's wrapper.
     */
    static function show($title, $content, $button_label="", $button_url="", $new_tab=false, $class="") {
        ob_start();
        ?>

        <div class="eweb_popups_wrapper<?php echo esc_attr(" " . $class); ?>">
            <div class="eweb_popups_background"></div>
            <div class="eweb_popups_box">
                <h1 class="eweb_popups_title"><?php echo esc_html($title); ?></h1>
                <div class="eweb_popups_content"><?php echo wp_kses_post($content); ?></div>
                <?php if ($button_label != "") : ?>
                    <a class="eweb_popups_button" <?php if ($button_url != "") : ?>href="<?php echo esc_url($button_url); ?>" <?php endif; ?> <?php if ($new_tab) echo (' ' . 'target="_blank" rel="noopener noreferrer"'); ?> tabindex="0">
                        <?php echo esc_html($button_label); ?>
                    </a>
                <?php endif; ?>
                <button class="eweb_popups_close"><?php echo esc_html__("Close"); ?>
            </div>
        </div>

        <?php
        $output = ob_get_contents();
        ob_end_clean();

        echo $output;
    }

    static function load_resources() {
        wp_enqueue_script("eweb-popups", plugin_dir_url(__FILE__) . "res/scripts/main.js", array("jquery"), '', true);
        wp_enqueue_style("eweb-popups", plugin_dir_url(__FILE__) . "res/styles/main.css");
    }
}

add_action("wp_enqueue_scripts", "EWEB\\Popups::load_resources");
